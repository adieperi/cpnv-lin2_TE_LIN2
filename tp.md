Nom : Dieperink
Prénom : Anthony
Classe : SIT1B
Date : 26.01.2023

Quelle est la commande pour exécuter le fichier docker-compose.yaml ?
```
docker-compose up -d
```
Quelle est la commande pour se connecter avec un terminal et interagir avec l'application serveur ?
```
docker attach lin2-server
```
Quelle est la commande pour se connecter avec un terminal et interagir avec l'application cliente ?
```
docker attach lin2-client
```
Quelle est la commande pour transformer le container serveur en une image ?
```
docker commit lin2-server adieperi/lin2-server:latest
```
Quelle est la commande pour pousser l'image précédemment créée sur dépôt personnel ?
```
docker push adieperi/lin2-server:latest
```
